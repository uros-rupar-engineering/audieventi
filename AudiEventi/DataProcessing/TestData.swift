//
//  TestData.swift
//  AudiEventi
//
//  Created by uros.rupar on 7/7/21.
//

import Foundation

class TestData{
    
    static var Publicevents:[PublicEvent] = []
    static var premiumEvents:PremiumEvent!
    static var audiExpirence:AudiDrivEexpirienceResponse!
    static var foodExpirience:FoodExperience!
    static var placeAndTeritories:PlacesAndTerritoryResponse!
    static var contactsAndInfo: ContatctsAndInfo!
    
    static func populateTestData(){
         fetcPublicEvent(jsonData)
        fetchPremiumEvent(jsonPremiumEvents)
        fetchAudiExpirience(jsonAudiPremiumExpirience)
        fetchFoodExpirience(foodExperienceJson)
        fetchPlaceAndTeritory(placesAndTerritoryJson)
        fetchContactAndInfo(contactAndInfoJSONData)
    }
    
    
    static func fetcPublicEvent(_ string:String){
        
      
        
//        let task =  URLSession.shared.dataTask(with: URL(string: url)!){
//            (data,response,error) in
//            if let data =  data , case let publicvent = try?
//
//
//                JSONDecoder().decode(PublicEvent.self, from: data){
//
//                completion(publicvent)
//            }
//        }
//
  //      task.resume()
        
        let jsonData  = string.data(using: .utf8)

        let  decoder = JSONDecoder()

        if let data = jsonData, let events = try? decoder.decode([PublicEvent].self, from: data){
       
            
            Publicevents = events
            
           
        }
        
      
        
//        let jsonStr2 = jsonData.data(using: .utf8)
//        if let data2 = jsonStr2, let events = try? JSONDecoder().decode(PublicEvent.self, from: data2) {
//
//
//
//            print(events)
//
//
//
//        }
        
    }
    
    
    static func fetchPremiumEvent(_ string:String){
        
        
        let jsonData  = string.data(using: .utf8)

        let  decoder = JSONDecoder()

        if let data = jsonData, let events = try? decoder.decode(PremiumEvent.self, from: data){
    print(events)
            
            premiumEvents = events
            
           
        }
        
    }
    
    static func fetchAudiExpirience(_ string:String){
        
        
        let jsonData  = string.data(using: .utf8)

        let  decoder = JSONDecoder()

        if let data = jsonData, let events = try? decoder.decode(AudiDrivEexpirienceResponse.self, from: data){
       print(events)
            
            audiExpirence = events
            
           
        }
        
    }
    
    static func fetchFoodExpirience(_ string:String){
        
        
        let jsonData  = string.data(using: .utf8)

        let  decoder = JSONDecoder()

        if let data = jsonData, let events = try? decoder.decode(FoodExperience.self, from: data){
       print(events)
            
            foodExpirience = events
            
           
        }
        
    }
    
    static func fetchPlaceAndTeritory(_ string:String){
        
        
        let jsonData  = string.data(using: .utf8)

        let  decoder = JSONDecoder()

        if let data = jsonData, let events = try? decoder.decode(PlacesAndTerritoryResponse.self, from: data){
       print(events)
            
            placeAndTeritories = events
            
           
        }
        
    }
    
    static func fetchContactAndInfo(_ string:String){
        
        
        let jsonData  = string.data(using: .utf8)

        let  decoder = JSONDecoder()

        if let data = jsonData, let events = try? decoder.decode(ContatctsAndInfo.self, from: data){
       print(events)
            
            contactsAndInfo = events
            
           
        }
        
    }
    
   static let jsonData =  """
[{
    "id": "1",
    "title": "Audi Talks Performance: ieri, oggi, e domani.",
    "description": {
        "format": "",
        "value": "Otto puntate con ospiti d'eccezione per condividare visioni all'avangurdia e discutere di una nouva, straordinaria idea"

    },
    "dataEvent": "02.09.2021",
    "linkMyAudi": {
        "uri": "www.audi.de",
        "title": "Audi",
        "options": []

    },
    "priority": 4,
    "status": "OK",
    "backgroundImage": {
        "id": "1",
        "href": "https://cdn-rs.audi.at/media/ThreeColTeaser_TextImage_Image_Component/12754-paragraphs-75119-51281-image/dh-925-bc764f/ae4a3c6b/1622795799/1920x1080-exterior-aq7-191010-oe.jpg",
        "meta": {
            "alt": "loading..",
            "title": "Meta",
            "width": 200,
            "height": 300
        }
    }
}, {
    "id": "2",
    "title": "Cortina e-portrait",
    "description": {
        "format": "",
        "value": "Dal 2017 Audi - per tutilare Cortina come patrimonio si e fatta promotrice con il Comune di Ampezzo di un progetto di Corporate..."

    },
    "dataEvent": "03.09.2021",
    "linkMyAudi": {
        "uri": "www.audi.de",
        "title": "Audi",
        "options": []

    },
    "priority": 1,
    "status": "OK",
    "backgroundImage": {
        "id": "2",
        "href": "https://autoblog.rs/gallery/108/201217-audi%20a3%2033.jpg",
        "meta": {
            "alt": "loading..",
            "title": "Meta",
            "width": 200,
            "height": 300
        }
    }
},{
    "id": "3",
    "title": "Mountaineering Workshop con Herve ...",
    "description": {
        "format": "",
        "value": "Un evento dedicato alla montagna e al guisto approccio per viverla con rispetto e inteligenza, dalla attivita sportive come lo sci ..."

    },
    "dataEvent": "03.09.2021",
    "linkMyAudi": {
        "uri": "www.audi.de",
        "title": "Audi",
        "options": []

    },
    "priority": 2,
    "status": "OK",
    "backgroundImage": {
        "id": "2",
        "href": "https://cdn-rs.audi.at/media/Theme_Menu_Model_Dropdown_Item_Image_Component/root-rs-model-modelMenu-editableItems-13719-dropdown-314510-image/dh-478-a0e9a6/9134d247/1622795873/1920x1920-audi-q5-sportback-my2021-1342-big-oe.jpg",
        "meta": {
            "alt": "loading..",
            "title": "Meta",
            "width": 200,
            "height": 300
        }
    }
},{
    "id": "4",
    "title": "Performance Workshop con Kristian Ghedina",
    "description": {
        "format": "",
        "value": "I pluricampioni Kristian Ghedina e Peter Fill, insieme agli chef stellati Costardi Bros, ti guideranno in un 'esperienza totale, tra sport."

    },
    "dataEvent": "03.09.2021",
    "linkMyAudi": {
        "uri": "www.audi.de",
        "title": "Audi",
        "options": []

    },
    "priority": 3,
    "status": "OK",
    "backgroundImage": {
        "id": "2",
        "href": "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/18_audi_a4_e-tron_rs_render_2021.jpg?itok=vouYnOJ0",
        "meta": {
            "alt": "loading..",
            "title": "Meta",
            "width": 200,
            "height": 300
        }
    }
}

]
"""
    
static let jsonPremiumEvents = """
    {
        "data": [{
            "id": "1",
            "title": "Uomini fiesta del Audi",
            "description": {
                "value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                "format": "nn",
                "processed": "Y"
            },
            "headerPremium": "Bene bene",
            "linkMyAudiPremium": {
                "uri": "www.audi.de",
                "title": "Audi",
                "options": []
            },
            "noteProgram": {
                "value": "Culinigo umelana",
                "format": "Som",
                "processed": "Uno"
            },
            "programDetails": [{
                "day": "1",
                "activities": [{
                    "start": "16:30",
                    "end": "20:30",
                    "activity": "Survey disponibile"
                }]
            }],
            "subtitle": "Madona di Camiglio",
            "image": {
                "id": "1",
                "href": "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/18_audi_a4_e-tron_rs_render_2021.jpg?itok=vouYnOJ0",
                "meta": {
                    "alt": "loading..",
                    "title": "Meta",
                    "width": 200,
                    "height": 300
                }
            }
        }],
        "result": "200",
        "resultCode": "200",
        "resultMessage": "OK"
    }
    """
    
   static let jsonAudiPremiumExpirience = """
            
            {

                "data": [{
                    "id": "1",
                    "description": "È universalmente riconosciuto che un lettore che osserva il layout di una pagina viene distratto dal contenuto testuale se questo è leggibile. Lo scopo dell utilizzo del Lorem Ipsum è che offre una normale distribuzione delle lettere (al contrario di quanto avviene se si utilizzano brevi frasi ripetute, ad esempio testo qui), apparendo come un normale blocco di testo leggibile. Molti software di impaginazione e di web design utilizzano Lorem",

                    "subtitleAde": "Al contrario di quanto si pensi, Lorem Ipsum ",

                    "titleAde": "Audi driving Experience",

                    "sliderImage": [{
                            "id": "2",
                            "href": "https://autoblog.rs/gallery/108/201217-audi%20a3%2033.jpg",
                            "meta": {
                                "alt": "loading..",
                                "title": "Meta",
                                "width": 200,
                                "height": 300
                            }
                        }, {
                            "id": "2",
                            "href": "https://c4.wallpaperflare.com/wallpaper/675/310/349/audi-rs6-audi-rs6-avant-quattro-wallpaper-preview.jpg",
                            "meta": {
                                "alt": "loading..",
                                "title": "Meta",
                                "width": 200,
                                "height": 300
                            }
                        }, {
                            "id": "2",
                            "href": "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/18_audi_a4_e-tron_rs_render_2021.jpg?itok=vouYnOJ0",
                            "meta": {
                                "alt": "loading..",
                                "title": "Meta",
                                "width": 200,
                                "height": 300
                            }
                        }, {
                            "id": "2",
                            "href": "https://audiclubserbia.com/club/wp-content/uploads/2021/02/1-audi-e-tron-s-sportback-2021-uk-first-drive-review-hero-front-450x290@2x.jpg",
                            "meta": {
                                "alt": "loading..",
                                "title": "Meta",
                                "width": 200,
                                "height": 300
                            }
                        }

                    ],

                    "imageAde": {
                        "id": "2",
                        "href": "https://topspeed.rs/gallery/thumb/foto-audi-1/page/1/marker/30868/photo/167060.jpeg",
                        "meta": {
                            "alt": "loading..",
                            "title": "Meta",
                            "width": 200,
                            "height": 300
                        }
                    }


                }],
                "result": "200",
                "resultCode": "200",
                "resultMessage": "OK"


            }
            
        """
    
    
    static let foodExperienceJson = """
    {
        "data": [{
            "id": "1",
            "title": "TITLE1",
            "header": "Jjaiojai najo",
            "foodSubtitle": {
                "value": "String",
                "format": "",
                "processed": ""
            },
            "foodImage": {
                "id": "1",
                "href": "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/18_audi_a4_e-tron_rs_render_2021.jpg?itok=vouYnOJ0",
                "meta": {
                    "alt": "loading..",
                    "title": "Meta",
                    "width": 200,
                    "height": 300
                }
            },
            "programExperience": [
                {
                    "day": "11.01.2021.",
                    "start": "08:00",
                    "type": "",
                    "activity": "brekfast",
                    "site": "",
                    "description": "",
                    "food": "",
                    "alergens": ""
                },
                {
                    "day": "21.02.2021.",
                    "start": "",
                    "type": "",
                    "activity": "",
                    "site": "",
                    "description": "",
                    "food": "",
                    "alergens": ""
                }
            ]
        }],
        "result": "200",
        "resultCode": "200",
        "resultMessage": "OK"
    }
    """
    
    
    static let placesAndTerritoryJson =
        """
    {
        "data": [{
            "id": "1",
            "title": "Uno Due Tre Divergenti",
            "description": "Suspendisse auctor at nulla a ultrices. Suspendisse accumsan diam vitae eleifend blandit. Praesent a augue faucibus, feugiat diam ac, semper elit. Suspendisse et semper quam, quis euismod tortor. Integer congue ut risus id interdum. Fusce dapibus metus nec augue porta, blandit scelerisque sem sagittis. Maecenas interdum turpis vitae tortor euismod efficitur vitae sed massa.",
            "placeSubtitle": "Cognos",
            "placeTitle": "Nepal",
            "placesSlider": [{
                "id": "1",
                "href": "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/18_audi_a4_e-tron_rs_render_2021.jpg?itok=vouYnOJ0",
                "meta": {
                    "alt": "loading..",
                    "title": "Meta",
                    "width": 200,
                    "height": 300
                }
            }],
            "imagePlace": {
                "id": "1",
                "href": "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/18_audi_a4_e-tron_rs_render_2021.jpg?itok=vouYnOJ0",
                "meta": {
                    "alt": "loading..",
                    "title": "Meta",
                    "width": 200,
                    "height": 300
                }
            }
        }],
        "result": "200",
        "resultCode": "200",
        "resultMessage": "OK"
    }
    """
    
    let surveyResponseJSONData = """
            {
                  "questions": [{
                        "webformId": "1",
                        "title": "Random title",
                        "questionId": "1",
                        "required": "Required",
                        "requiredError": "Required Error",
                        "webformMultiple": "Web Form",
                        "type": "Type"
                            }],
                  "status": "Accepted",
                  "result": "200",
                  "resultCode": "200",
                  "resultMessage": "OK"

            }
        """
    
   static let contactAndInfoJSONData = """
            {
                  "data": [{
                        "id": "1",
                        "title": "Some random Title"
                            }],
                  "result": "200",
                  "resultCode": "200",
                  "resultMessage": "OK"

            }
    """

}
