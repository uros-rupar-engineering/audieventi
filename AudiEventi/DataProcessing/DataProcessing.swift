//
//  DataProcessing.swift
//  AudiEventi
//
//  Created by uros.rupar on 7/2/21.
//

import Foundation
import UIKit

class DataProcessing{
    static let baseUrl = "http://mobileapp-coll.engds.it/AudiEventi/"
    static let publicEventStr = "/audi/api/public/"
    static let langs = "langs/it_IT.json"
    
    static func makeUrl(_ baseUrl: String, _ route: String)-> String{
        let fullUrl = baseUrl + route
        
        return fullUrl
    }
    
    static var langsObject:Langs?
    
    static func fetchLangs(url: String, completion: @escaping (Langs?) -> Void){
        
        guard let apiURL = URL(string: url) else { return }
        let task = URLSession.shared.dataTask(with: apiURL){ (data, response, error) in
            
            if let error = error {
                print("Error accesing Audi Api: \(error)")
                
            }
 
            guard let httpResponse = response as? HTTPURLResponse,
                  
                  (200...299).contains(httpResponse.statusCode) else {
                
                print("Error with response, uninspected status code: \(response)")
                
                return
                
            }
            if let data = data, case let langs = try? JSONDecoder().decode(Langs.self, from: data){
                completion(langs)
            }
        }
        task.resume()
    }
    
    func prepareImage(url:String)->UIImage{
        var image = UIImage()
        if let url = URL(string: url){
            
            URLSession.shared.dataTask(with: url){(data, response, error) in
                if let data = data{
                    image = UIImage(data: data)!
                }
            }
                
            }
        return image
        }
    
}
