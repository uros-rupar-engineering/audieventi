//
//  Protocols.swift
//  AudiEventi
//
//  Created by uros.rupar on 7/2/21.
//

import Foundation

protocol MenuDelegate {
    func menuHandler(index : Int)
}
