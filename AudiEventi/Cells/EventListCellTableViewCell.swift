//
//  EventListCellTableViewCell.swift
//  AudiEventi
//
//  Created by uros.rupar on 7/7/21.
//

import UIKit

class EventListCellTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var headImage: UIImageView!
    
    @IBOutlet weak var TitleLabel: UILabel!
    
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var actionButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
