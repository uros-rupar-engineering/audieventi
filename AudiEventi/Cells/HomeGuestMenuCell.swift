//
//  HomeGuestMenuCell.swift
//  AudiEventi
//
//  Created by uros.rupar on 7/5/21.
//

import UIKit

class HomeGuestMenuCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var menuItemImg: UIImageView!
    
    @IBOutlet weak var menuItem: UILabel!
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
