//
//  AudiDriveexpirience.swift
//  AudiEventi
//
//  Created by uros.rupar on 7/1/21.
//

import Foundation

struct AudiDrivEexpirienceResponse:Codable,ResultProtocol{
    var data:[AudiDrivEexpirience]
    var result: String
    
    var resultCode: String
    
    var resultMessage: String
}

struct AudiDrivEexpirience:Codable{
    var id: String
    var description: String
    var subtitleAde: String
    var titleAde: String
//    var sliderImage: [BackgroundImage]
//    var imageAde: BackgroundImage
}
