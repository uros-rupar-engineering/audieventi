//
//  CouponModel.swift
//  AudiEventi
//
//  Created by uros.rupar on 7/2/21.
//

import Foundation

struct CouponVerification: Codable,ResultProtocol,CouponProtocol{
var Coupon: String
var EventId: String
var Status: String
var Valid: String
var EventStatus:String
var result: String
var resultCode: String
var resultMessage: String

}

struct CouponValidation{
    var Action: String
    var Coupon: String
    var EventId: String
    var Status: String
    var Valid: String
}
