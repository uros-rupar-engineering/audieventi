//
//  Survey.swift
//  AudiEventi
//
//  Created by uros.rupar on 7/1/21.
//

import Foundation

struct SurveyResponse:Codable,ResultProtocol{
    var questions:[Question]
    var result: String
    
    var resultCode: String
    
    var resultMessage: String}

struct Question:Codable {
    var webformId: String
    var title:String
    var questionI:String
    var required:String
    var requiredError: String
    var webformMultiple: String
    var type :String
    var webformLikert:String
 // is it needed
//    var result :String
//    var resultCode:String
//    var resultMessage: String
    
}


struct SurveyPost:Codable{
    var options:[String:String]
    var questions:String
    var other: String
    var answers: String
    
    
}
