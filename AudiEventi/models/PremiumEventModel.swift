//
//  DescriptionModel.swift
//  AudiEventi
//
//  Created by uros.rupar on 7/1/21.
//

import Foundation

struct PremiumEvent:Codable,ResultProtocol{
   
    
    var data: [PremiumEventData]
    
    var result: String
    
    var resultCode: String
    
    var resultMessage: String
    
}

struct PremiumEventData: Codable{
    var id:String
    var title:String
    var description:PremiumcDescription
    var headerPremium:String
    var linkMyAudiPremium:LinkMyAudi
    var noteProgram: NoteProgram
    var programDetails:[Day]
    var subtitle:String
    var image: BackgroundImage?
}

struct PremiumcDescription:Codable {
    var value:String
    var format:String

}

struct NoteProgram:Codable{
    var value:String
    var format:String
    var processed: String // html tags
}

struct Day:Codable {
    var day:String
    var activities:[Activity]
}

struct Activity:Codable {
    var start : String
    var end :String
    var activity: String
}
