//
//  Model.swift
//  AudiEventi
//
//  Created by uros.rupar on 7/1/21.
//

import Foundation

struct PublicEvent: Codable{
    var id: String
    var title: String
    var description:PublicDescription
    var dataEvent:String
    var linkMyAudi : LinkMyAudi
   var priority : Int
    var status : String
    var backgroundImage:BackgroundImage?
}

struct PublicDescription: Codable{
    var format: String
    var value:String
}


struct LinkMyAudi: Codable{
    var uri:String
    var title:String
    var options :[Option] //what
}

struct Option: Codable{
    
}

struct BackgroundImage: Codable {
    var id: String
    var href: String
    var meta: Meta
}

struct Meta: Codable{
    var alt: String
    var title: String
    var width : Int
    var height: Int
}
