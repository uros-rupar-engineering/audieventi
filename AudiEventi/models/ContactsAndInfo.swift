//
//  ContactsAndInfo.swift
//  AudiEventi
//
//  Created by uros.rupar on 7/1/21.
//

import Foundation

struct ContatctsAndInfo:Codable,ResultProtocol {
    var data: [ContatctsAndInfoData]
    var result: String
    
    var resultCode: String
    
    var resultMessage: String}

struct ContatctsAndInfoData :Codable{
    var id: String
    var title: String
    //var contactTitle:ContactTitle
    //var freeText
    //var contactImage: BackgroundImage
}

// struct ContactTitle: Codable{}
