//
//  PlacesAndterritoryResponse.swift
//  AudiEventi
//
//  Created by uros.rupar on 7/1/21.
//

import Foundation

struct PlacesAndTerritoryResponse: Codable ,ResultProtocol{
    var data:[PlacesAndteritory]
    var result: String
    
    var resultCode: String
    
    var resultMessage: String
}

struct PlacesAndteritory:Codable{
    
    var id: String
    var title: String
    var description: String
    var placeSubtitle: String
    var placeTitle: String
    var placesSlider: [BackgroundImage]
    var imagePlace: BackgroundImage
}
