//
//  AudiEventi.swift
//  AudiEventi
//
//  Created by uros.rupar on 7/6/21.
//

import UIKit

class AudiEventi: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
   
    override func viewWillLayoutSubviews() {
        let width = self.view.frame.width
        let navigationBar: UINavigationBar = UINavigationBar(frame: CGRect(x: 0, y: self.view.safeAreaInsets.top, width: width, height: 44))
        self.view.addSubview(navigationBar);
        let navigationItem = UINavigationItem(title: "")
        let menu = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(showBurgerMenu))
        
        let notification =  UIBarButtonItem(title: "", style: .done, target: self, action: nil)
        navigationItem.leftBarButtonItem = menu
        navigationItem.leftBarButtonItem?.tintColor = .black
        
        navigationItem.rightBarButtonItem = notification
        navigationItem.leftBarButtonItem?.tintColor = .black
        
        navigationController?.navigationBar.barTintColor = .white;        navigationItem.leftBarButtonItem?.image = UIImage(named: "menu")
        navigationItem.rightBarButtonItem?.image = UIImage(named: "notification_no_badge")
        let image: UIImage = UIImage(named: "logo.png")!
        let imageView = UIImageView(frame: CGRect(x: 0, y: 30, width: 40, height: 40))
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        navigationItem.titleView = imageView
        navigationBar.setItems([navigationItem], animated: false)
       }
    
    // Display burger
    func presentDetail(_ vc:UIViewController){
        let transition = CATransition()
        transition.duration = 0.5
          
        view.window?.layer.add(transition, forKey: kCATransition)

        vc.modalPresentationStyle = .overCurrentContext

        present(vc, animated:false,completion: nil)
    }
    
    @objc func showBurgerMenu(){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard?.instantiateViewController(identifier: "MenuViewController") else { return  }
        
        presentDetail(vc)
        
    }

    @objc func showEventiList(_ viewController: String){
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard?.instantiateViewController(identifier: viewController) else { return  }
        vc.modalPresentationStyle = .overCurrentContext
        present(vc, animated:false,completion: nil)
        
    }
    
    
    

    
    


}
