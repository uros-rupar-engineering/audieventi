//
//  EventiViewController.swift
//  AudiEventi
//
//  Created by uros.rupar on 7/7/21.
//

import UIKit

class EventiViewController: AudiEventi,UITableViewDelegate,UITableViewDataSource {
  
    

    override func viewDidLoad() {
        super.viewDidLoad()
       print("dddd")
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Events"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        TestData.Publicevents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        tableView.rowHeight = tableView.frame.height * 0.5
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventlistcell",for: indexPath) as? EventListCellTableViewCell
        
        if let urlString = TestData.Publicevents[indexPath.row].backgroundImage?.href as? String, let url = URL(string: urlString){
            URLSession.shared.dataTask(with: url){ (data,response,error) in
                if let data = data{
                    DispatchQueue.main.async {
                        cell?.headImage.image = UIImage(data: data)
                        
                        
                    }
                }
            }.resume()
            
        }
        
        
        cell?.TitleLabel.text = TestData.Publicevents[indexPath.row].title
        cell?.descriptionLabel.text = TestData.Publicevents[indexPath.row].description.value
        
        return cell!
    }
  
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
