//
//  MenuViewController.swift
//  AudiEventi
//
//  Created by uros.rupar on 7/2/21.
//

import UIKit

class MenuViewController: AudiEventi,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var menuItemsTable: UITableView!
    
    let coupon = "1234"
    var couponTextField:UITextField = UITextField()
    
    
    var list:[String] = []
    var images:[String] = ["menu_home","menu_calendar","menu_coupon_eventi","menu_impostazioni"]
    
    @IBOutlet weak var areaForDissmisMenu: UIView!
    
    @IBOutlet weak var menuView: UIView!
    
 
    
    override func viewDidLoad() {
        
        menuItemsTable.dropShadow()
   
        menuView.layer.zPosition = 1
        super.viewDidLoad()
        DataProcessing.fetchLangs(url: DataProcessing.makeUrl(DataProcessing.baseUrl, DataProcessing.langs), completion: {(result) in
                                    guard let result = result else{ return}
            
            print("result")
            
            
            //print(data)
            
            self.list.append(result.SIDE_MENU_GUEST_HOME_PAGE)
            self.list.append(result.SIDE_MENU_GUEST_EVENT_LIST)
            self.list.append(result.SIDE_MENU_GUEST_COUPON_LOGIN)
            self.list.append(result.SIDE_MENU_GUEST_SETTINGS)
            print(self.list)
            
     
            DispatchQueue.main.async {
                self.menuItemsTable.reloadData()
            }
            
            
        })
        self.activateGestureDissmisForBurgerVc()
        self.menuItemsTable.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("----------",list.count)
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuItemCell", for: indexPath) as! HomeGuestMenuCell
        
        tableView.rowHeight = menuItemsTable.frame.height * 0.1
        cell.menuItem.text = list[indexPath.row]
        cell.menuItemImg.image = UIImage(named:images[indexPath.row])
        print(tableView.indexPathForSelectedRow)
        
        
                return cell
    }
    func activateGestureDissmisForBurgerVc(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(MenuViewController.tappedOnScreen(_:)))
        
        areaForDissmisMenu.addGestureRecognizer(tap)
    }

    
    @objc func tappedOnScreen(_ sender: UIGestureRecognizer) {

            let tapped = sender.location(in: view)

            let screenWidth = UIScreen.main.bounds.width

            if  tapped.x > screenWidth - 80 && tapped.y > 0 {

                let transition = CATransition()

                transition.duration = 0.5

                view.window?.layer.add(transition, forKey: kCATransition)

                dismiss(animated: false, completion: nil)

            }

        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        navigateToViewController(actions[indexPath.row])
    }
    
    
    
    let actions:[String] = ["home","eventi","coupon"]
    
    func navigateToViewController(_ action:String){
        
        switch action {
        case "eventi":
            showEventiList("EventiViewController")
        case "coupon":
            showAlertForCoupon()
        default:
            showEventiList("ViewController")
        }
    }
    
    @objc func showAlertForCoupon(){
        let alert = UIAlertController(title: "Coupone", message: "unsei kupon", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Enter your coupon", style: UIAlertAction.Style.default, handler: <#T##((UIAlertAction) -> Void)?##((UIAlertAction) -> Void)?##(UIAlertAction) -> Void#>))
        alert.addTextField{(textField) in
            textField.placeholder = "Audi12432"
            self.couponTextField = textField
        }
        self.present(alert, animated: true)
        
    }
    
    func goToPremium(_ enteredCoupon:String){
        
        if enteredCoupon == coupon{
            navigateToViewController("EventiViewController")
        }
    }

}

