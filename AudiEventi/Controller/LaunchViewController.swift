//
//  LaunchViewController.swift
//  AudiEventi
//
//  Created by uros.rupar on 7/8/21.
//

import UIKit

class LaunchViewController:  UIViewController{

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        

        DispatchQueue.main.async {
    
            sleep(2)

            self.launchApp()
  
    
        }
    }
    
    func launchApp(){
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard?.instantiateViewController(identifier: "ViewController") else { return  }
        vc.modalPresentationStyle = .overCurrentContext
        present(vc, animated:false,completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
